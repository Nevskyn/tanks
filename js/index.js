const cvs = document.getElementById("canvas");
const ctx = cvs.getContext("2d");

const tank = new Image();
const bg = new Image();
const fire = new Image();

bg.src = "img/bg.png";
tank.src = "img/tank.png";
fire.src = "img/fire.png";

const move = ({ code }) => {
  if (code === 'ArrowUp') {
    y -= 10;
  } else if (code === 'ArrowDown') {
    y += 10;
  } else if (code === 'ArrowLeft') {
    x -= 10;
  } else if (code === 'ArrowRight') {
    x +=10;
  }
}
document.addEventListener("keydown", move);


let x = 100;
let y = 250;
let shoot = 2;
let yShoot = 250;

const draw = () => {
  if (yShoot < 0) {
    yShoot = 250;
  }
  ctx.drawImage(bg, 0, 0);
  ctx.drawImage(tank, x, y);
  ctx.drawImage(fire, x+5, yShoot-25);
  yShoot -= shoot;
  requestAnimationFrame(draw);
}


fire.onload = draw;
